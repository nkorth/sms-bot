"""phone_cmd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from django.contrib import admin
from homepage.views import index
from mobile.views import mail_hook, test

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^mail_hook$', mail_hook),
    url(r'^test$', test),
    url(r'^$', index),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
