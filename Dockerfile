FROM python:3
ENV PYTHONUNBUFFERED 1
COPY . /code
WORKDIR /code
RUN chmod +x entrypoint.sh
RUN mkdir /cfg
RUN ln -s /cfg/secret_key.txt .
RUN ln -s /cfg/mailgun_api_key.txt .
RUN ln -s /cfg/darksky_api_key.txt .
RUN pip install -r requirements.txt
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["./entrypoint.sh"]
EXPOSE 80
