from django.conf import settings
from pyzipcode import ZipCodeDatabase
import requests

from .help import help

def weather(zip_code=None, profile=None):
    if not zip_code:
        if profile and profile.zip_code:
            zip_code = profile.zip_code
        else:
            return help('weather')

    try:
        zcdb = ZipCodeDatabase()
        entry = zcdb[zip_code]
        city = entry.place
        state = entry.state
        latitude = entry.latitude
        longitude = entry.longitude
    except IndexError:
        return ("Sorry, I couldn't find that location. "
                "Only 5-digit zip codes will work currently.")

    if profile:
        profile.zip_code = zip_code
        profile.save()

    url = "https://api.darksky.net/forecast/{}/{},{}".format(
            settings.DARKSKY_API_KEY, latitude, longitude)
    r = requests.get(url)
    r.raise_for_status()
    forecast = r.json()
    today = forecast['daily']['data'][0]

    today['summary'] = today['summary'].replace(u"\u2013", "-") # replace en-dash

    return (u"{}, {}: High of {:.0f}F, low of {:.0f}F. "
            u"Feels like {:.0f}F now. {} "
            u"\n(via darksky.net)").format(
                city, state, today['temperatureMax'], today['temperatureMin'],
                forecast['currently']['apparentTemperature'], today['summary'])

weather.args = "[ZIP_CODE]"
weather.help_text = ("Enter a zip code to see today's weather forecast for "
                     "that location. I'll remember the last zip code you used, "
                     "so you don't have to type it every time.")
