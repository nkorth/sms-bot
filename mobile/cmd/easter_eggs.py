from .common import get_profile, not_found

def make(*args, **kwargs):
    if args == ('me', 'a', 'sandwich'):
        return "What? Make it yourself."
    else:
        return not_found('make')

def sudo(*args, **kwargs):
    if args == ('make', 'me', 'a', 'sandwich'):
        if kwargs['profile']:
            profile = kwargs['profile']
            profile.found_sudo = True
            profile.save()
        return "Okay."
    else:
        return not_found('sudo')
