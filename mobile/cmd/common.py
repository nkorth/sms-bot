from mobile.models import Profile

def find_command(name):
    #TODO match prefixes too
    try:
        return commands[name.lower()]
    except:
        return hidden_commands[name.lower()]

def get_profile(user):
    try:
        return user.profile
    except Profile.DoesNotExist:
        new_profile = Profile()
        new_profile.user = user
        new_profile.save()
        return new_profile

def not_found(command):
    return ("Command not found: \"{}\". \n"
            "Enter \"help\" for a list of commands.").format(command)

def command_list():
    return ", ".join(sorted(commands.keys()))


from .easter_eggs import make, sudo
from .explain import explain
from .help import help
from .mbus import mbus
from .turnip import turnip
from .weather import weather

commands = {
    'explain': explain,
    'help': help,
    'mbus': mbus,
    'weather': weather,
}

hidden_commands = {
    'man': help,
    'make': make,
    'sudo': sudo,
    'turnip': turnip,
}
