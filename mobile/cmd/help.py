from .common import find_command, command_list

def help(command=None, profile=None):
    if command:
        if command.lower() == "command":
            return ("When I said \"help COMMAND\", I meant you should replace "
                    "COMMAND with the name of a command. Sorry for the "
                    "confusion!")

        try:
            command_fn = find_command(command)
            return "Usage: {} {} \n{}".format(
                    command, command_fn.args, command_fn.help_text)
        except AttributeError:
            try:
                command_fn = find_command(command)
                return "Usage: {} {}".format(
                        command, command_fn.args)
            except AttributeError:
                # this happens if the command either doesn't exist or
                # isn't documented
                return ("It seems there's no help entry for "
                        "\"{}\" yet.").format(command)
    else:
        return ("Available commands: {}. \n"
                "Use \"help COMMAND\" for details.").format(command_list())

help.args = "[COMMAND]"
help.help_text = ("Send \"help\" on its own to see a list of all commands. "
                  "Or, you can specify a command for details on how to use it.")
