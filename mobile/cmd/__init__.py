from .common import find_command, get_profile, not_found
from .help import help

def execute(query, user=None):
    if user:
        profile = get_profile(user)
        profile.last_query = query
        profile.save()
    else:
        profile = None

    argv = query.strip().split()
    try:
        command_fn = find_command(argv[0])
        return command_fn(*argv[1:], profile=profile)
    except KeyError:
        return not_found(argv[0])
    except IndexError:
        return help()
    except TypeError as e:
        print(e)
        return help(argv[0])
