from bs4 import BeautifulSoup
import requests

from .help import help

def _strip_tags(el):
    return "".join([child for child in el.recursiveChildGenerator()
                            if isinstance(child, str)])

def _get_wiki_page(title):
    url = "https://en.wikipedia.org/w/api.php"
    payload = {
        'action': "parse",
        'page': title,
        'prop': "text",
        'redirects': "1",
        'format': "json"
    }
    headers = {'user-agent': ("SMSBot/0.1 (http://cmd.nkorth.com; "
                              "webmaster@nkorth.com)")}
    request = requests.get(url, params=payload, headers=headers).json()
    if 'error' in request:
        raise LookupError(request['error']['info'])
    return request['parse']['text']['*'].replace("\n", "")

def explain(*args, **kwargs):
    if len(args) == 0:
        return help("explain")

    title = " ".join(args)
    try:
        html = BeautifulSoup(_get_wiki_page(title))
    except LookupError as e:
        return str(e)

    if len(html.findAll(id="disambigbox")) > 0:
        return "Be more specific?"

    # remove some extraneous elements
    for el in html.findAll("p", "mw-empty-elt"): el.extract()
    for el in html.findAll("table", "infobox"): el.extract()
    for el in html.findAll("div", "toc"): el.extract()
    for el in html.findAll("sup", "reference"): el.extract()
    for el in html.findAll("sup", "noprint"): el.extract()
    for el in html.findAll(id="coordinates"):
        for p in el.findParents("p"):
            p.extract()

    first_paragraph = html.findAll("p")[0]
    result = _strip_tags(first_paragraph)
    first_sentence_end = result.find(". ", 60) # should probably be at least 60 chars in
    if first_sentence_end != -1:
        return result[:first_sentence_end+1]
    else:
        return result

explain.args = "TOPIC"
explain.help_text = ("Enter a topic to get a brief explanation of it from "
                     "Wikipedia.")
