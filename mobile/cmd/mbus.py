from bs4 import BeautifulSoup
import datetime, requests

def mbus(direction, stop=None, profile=None):
    if not stop:
        # if the user didn't put a space between the direction and the stop
        stop_code = direction
    else:
        stop_code = direction+stop

    if stop_code.lower() in ("e604", "e603"):
        return ("To be picked up from this location, please call Transit "
                "Services at 734-936-6641.")

    buses = []

    try:
        r = requests.get("http://pts.umich.edu/stops/?s={}".format(stop_code))
        r.raise_for_status()
        html = BeautifulSoup(r.text)
        routes_table = html.find("table", attrs={'summary':"Routes"})
        for tr in routes_table.findAll("tr", attrs={'class':"r2"}):
            route_name = tr.td.string
            times = [el.string for el in tr.nextSibling.findAll("td")]
            times[0] = times[0].replace(" p.m.", "pm").replace(" a.m.", "am")
            buses.append("{} at {}".format(route_name, times[0]))
        if len(buses) == 0:
            raise ValueError
        return "Scheduled arrivals: \n{}".format("; \n".join(buses))
    except requests.exceptions.HTTPError:
        return "Failed to retrieve bus times."
    except:
        result = "I couldn't find any bus times for that stop."
        hour = (datetime.datetime.utcnow().hour + 19) % 24 # UTC-5
        if hour >= 22 or hour < 7:
            result += (" \nNeed a ride after hours? SafeRide: 734-647-8000, "
                       "DPS (emergency only): 734-763-1131")
        return result

mbus.args = "STOP_CODE"
mbus.help_text = ("The stop code is written on a sign at the bus stop. "
                  "Eg. \"N 550\" \n"
                  "(Works only with Blue Buses at the University of Michigan)")
