from mobile.models import Profile
import datetime, math, random

def _get_price(date):
    seed = "{} {} {}".format(date.year, date.month, date.day)
    random.seed(seed)
    price = int(random.gauss(100, 20))
    return price

def _week_number(date):
    return date.isocalendar()[1]

def _get_score(profile):
    now = datetime.datetime.utcnow().date()
    if profile.turnip_last_reset != _week_number(now):
        profile.turnip_turnips = 0
        profile.turnip_bells = 1000
        profile.turnip_last_reset = _week_number(now)
        profile.save()
    return (profile.turnip_turnips, profile.turnip_bells)

def _ordinal(n):
    if 11 <= n <= 20:
        return "{:d}th".format(n)
    elif n % 10 == 1:
        return "{:d}st".format(n)
    elif n % 10 == 2:
        return "{:d}nd".format(n)
    elif n % 10 == 3:
        return "{:d}rd".format(n)
    else:
        return "{:d}th".format(n)

def turnip(action=None, n=None, profile=None):
    now = datetime.datetime.utcnow().date()
    price = _get_price(now)
    if action is None:
        if profile:
            turnips, bells = _get_score(profile)
            return ("Today's turnip price is {} Bells. \n"
                    "You have {} turnips and {} Bells.").format(
                            price, turnips, bells)
        else:
            return "Today's turnip price is {} Bells.".format(price)
    elif action.lower() == "buy":
        if not profile:
            return ("Sorry, you can't currently play the turnip game from the "
                    "anonymous web interface. Try it on your phone!")
        else:
            turnips, bells = _get_score(profile)
            try:
                n = int(n)
            except ValueError:
                return ("Usage: turnip buy N \n"
                        "where N is a whole number of turnips to purchase.")
            cost = n * price
            if cost <= bells:
                profile.turnip_turnips = turnips + n
                profile.turnip_bells = bells - cost
                profile.save()
                return ("You bought {} turnips. \n"
                        "You now have {} turnips and {} Bells.").format(
                                n, profile.turnip_turnips, profile.turnip_bells)
            else:
                return "You don't have enough Bells!"
    elif action.lower() == "sell":
        if not profile:
            return ("Sorry, you can't currently play the turnip game from the "
                    "anonymous web interface. Try it on your phone!")
        else:
            turnips, bells = _get_score(profile)
            try:
                n = int(n)
            except ValueError:
                return ("Usage: turnip sell N \n"
                        "where N is a whole number of turnips to sell.")
            if n <= turnips:
                profile.turnip_turnips = turnips - n
                profile.turnip_bells = bells + (n * price)
                profile.save()
                return ("You sold {} turnips. \n"
                        "You now have {} turnips and {} Bells.").format(
                                n, profile.turnip_turnips, profile.turnip_bells)
            else:
                return "You don't have that many turnips!"
    elif action.lower() == "rank":
        scores = [_get_score(p) for p in Profile.objects.all()]
        worths = sorted([s[0]*price + s[1] for s in scores])
        if profile:
            turnips, bells = _get_score(profile)
            my_worth = turnips*price + bells
            rank = len([w for w in worths if w > my_worth]) + 1
            tied = len([w for w in worths if w == my_worth]) - 1
            tied_text = "tied for" if tied else "in"
            if rank == 1:
                return "You are {} 1st place with a net worth of {} Bells!" \
                        .format(tied_text, my_worth)
            else:
                return ("You are {} {} place with a net worth of {} Bells. "
                        "The current leader has a net worth of {} Bells.").format(
                                tied_text, _ordinal(rank), my_worth, worths[0])
        else:
            return "The current leader has a net worth of {} Bells.".format(
                            worths[0])
    else:
        return "Usage: turnip; turnip buy N; turnip sell N; turnip rank"

turnip.args = "buy N; turnip sell N; turnip rank; turnip"
turnip.help_text = ("Play the stalk market in this weekly gambling game! "
                    "Every Monday, all players restart with 1000 Bells.")
