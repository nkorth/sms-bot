from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from .models import Profile
import hashlib, hmac, json, logging, requests, time

logger = logging.getLogger(__name__)

from . import cmd

def _mailgun_verify(request_data):
    """Returns true if the request data passes the authenticity test."""
    #now = time.time()
    #if not (now - 60 < request_data['timestamp'] < now + 10):
    #    return False # to prevent replay attacks
    return request_data['signature'] == hmac.new(
            key=settings.MAILGUN_API_KEY,
            msg="{}{}".format(request_data['timestamp'], request_data['token']),
            digestmod=hashlib.sha256).hexdigest()

def _mailgun_send(to, subject, body):
    payload = {'from': "SMS Bot <cmd@nkorth.com>", 'to': to, 'subject': subject, 'text': body}
    url = "https://api.mailgun.net/v3/nkorth.com/messages"
    r = requests.post(url, auth=("api", settings.MAILGUN_API_KEY), data=payload)
    r.raise_for_status()

def _get_or_create_user(request, email):
    try:
        request.user = User.objects.get(email=email)
    except User.DoesNotExist:
        new_user = User()
        new_user.username = email
        new_user.email = email
        new_user.save()
        new_profile = Profile()
        new_profile.user = new_user
        new_profile.save()
        request.user = new_user

@require_POST
@csrf_exempt
def mail_hook(request):
    if not _mailgun_verify(request.POST):
        return HttpResponseBadRequest("Submitted data failed the authenticity test.")

    user_email = request.POST['from']
    _get_or_create_user(request, user_email)
    subject = request.POST['subject']
    query = request.POST['stripped-text']

    if query.startswith("T-Mobile "): # fix for stupid sms gateway
        query = query[len("T-Mobile "):]

    try:
        response = cmd.execute(query, user=request.user)
    except Exception as e:
        response = "Something unexpectedly went wrong! Try again later."
        logger.error(e)

    for offset in range(0, len(response), 160):
        _mailgun_send(user_email, "", response[offset:offset+160])

    return HttpResponse("Message processed!")

def test(request):
    try:
        query = request.GET['q']
        result = cmd.execute(query)
        return HttpResponse(result, content_type="text/plain")
    except KeyError:
        return HttpResponseBadRequest("No command specified.", content_type="text/plain")
