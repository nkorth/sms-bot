from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    last_query = models.CharField(max_length=160, blank=True)
    zip_code = models.CharField(max_length=5, blank=True)
    found_sudo = models.BooleanField(default=False)
    turnip_turnips = models.IntegerField(default=0)
    turnip_bells = models.IntegerField(default=0)
    turnip_last_reset = models.IntegerField(default=-1)

    def phone_number(self):
        number, domain = self.user.email.split("@")
        if domain in self._carriers.keys():
            return number
        else:
            return None

    def carrier(self):
        number, domain = self.user.email.split("@")
        if domain in self._carriers.keys():
            return self._carriers[domain]
        else:
            return None

    _carriers = {
        "txt.att.net": "AT&T",
        "txt.republicwireless.com": "Republic Wireless",
        "messaging.sprintpcs.com": "Sprint",
        "tmomail.net": "T-Mobile",
        "vtext.com": "Verizon",
        "vmobl.com": "Virgin Mobile",
    }

    def __str__(self):
        return self.user.email
