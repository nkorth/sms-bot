from django.contrib import admin
import datetime

from .models import Profile

class CarrierListFilter(admin.SimpleListFilter):
    title = "carrier"

    parameter_name = "carrier"

    def lookups(self, request, model_admin):
        queryset = model_admin.get_queryset(request)
        for domain, carrier in Profile._carriers.items():
            if queryset.filter(user__email__endswith=domain).exists():
                yield (domain, carrier)

    def queryset(self, request, queryset):
        if self.value() in Profile._carriers.keys():
            return queryset.filter(user__email__endswith=self.value())

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ("__str__", "phone_number", "carrier",
            "last_query", "zip_code", "found_sudo", "playing_turnip")

    list_filter = (CarrierListFilter,)

    def playing_turnip(self, profile):
        now = datetime.datetime.now().date()
        week_number = now.isocalendar()[1]
        up_to_date = profile.turnip_last_reset == week_number
        return up_to_date and \
                (profile.turnip_turnips > 0 or profile.turnip_bells != 1000)
    playing_turnip.boolean = True
