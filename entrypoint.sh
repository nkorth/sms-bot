if [ ! -f /cfg/secret_key.txt ]; then
	echo "Generating secret key..."
	head -c 40 /dev/random | base64 > /cfg/secret_key.txt
fi
if [ ! -f /cfg/mailgun_api_key.txt ]; then
	echo "Missing /cfg/mailgun_api_key.txt!"
	exit
fi
if [ ! -f /cfg/darksky_api_key.txt ]; then
	echo "Missing /cfg/darksky_api_key.txt!"
	exit
fi
/usr/local/bin/gunicorn -b 0.0.0.0:80 phone_cmd.wsgi
